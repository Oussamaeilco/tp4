package com.example.tp4;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> {

    private List<RepoList> repolist;

    public RepositoryAdapter(List<RepoList> repolist){
        this.repolist=repolist;
    }

    @NonNull
    @Override
    public RepositoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout,parent,false);
        return new RepositoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepositoryViewHolder holder, int position) {
        holder.display(repolist.get(position));
    }

    @Override
    public int getItemCount() {
        return repolist.size();
    }

    class RepositoryViewHolder extends RecyclerView.ViewHolder{
        private TextView name;
        public RepositoryViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
        }
        void display(RepoList repoList){
            name.setText(repoList.getRepoName());
        }
    }

}

